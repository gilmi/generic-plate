-- Mostly taken from the uniplate library by Neil Mitchell

{-# LANGUAGE FlexibleContexts #-}

import Type
import Testset
import Data.Char
import Data.Foldable

import qualified Data.Generics.Uniplate.Data as U
import qualified Data.Generics.Plated as G

main :: IO ()
main = test



-- benchmark :: Benchmark
-- benchmark = Benchmark
--     variables_ zeros_ simplify_
--     rename_ symbols_ constFold_
--     (increase_ 100) (incrone_ "" 100) bill_

uvariables, gvariables :: Expr -> [String]
uvariables x = [y | Var y <- U.universe x]
gvariables x = [y | Var y <- G.universe x]

uzeros, gzeros :: Expr -> Int
uzeros x = length [() | Div _ (Val 0) <- U.universe x]
gzeros x = length [() | Div _ (Val 0) <- G.universe x]

usimplify, gsimplify :: Expr -> Expr
usimplify = U.transform simp
gsimplify = G.transform simp

simp (Sub x y)          = simp $ Add x (Neg y)
simp (Add x y) | x == y = Mul (Val 2) x
simp x                  = x

urename, grename :: Stm -> Stm
urename = U.transformBi rename_op
    where rename_op (V x) = V ("_" ++ x)
grename = G.transformBi rename_op
    where rename_op (V x) = V ("_" ++ x)

usymbols, gsymbols :: Stm -> [(Var,Typ)]
usymbols x = [(v,t) | SDecl t v <- U.universeBi x]
gsymbols x = [(v,t) | SDecl t v <- G.universeBi x]

uconstFold, gconstFold :: Stm -> Stm
uconstFold = U.transformBi const_op
    where
        const_op (EAdd (EInt n) (EInt m)) = EInt (n+m)
        const_op x = x
gconstFold = G.transformBi const_op
    where
        const_op (EAdd (EInt n) (EInt m)) = EInt (n+m)
        const_op x = x


uincrease, gincrease :: Integer -> Company -> Company
uincrease = uincreaseAny
gincrease = gincreaseCompany

uincreaseAny :: U.Biplate a Salary => Integer -> a -> a
uincreaseAny k = U.transformBi (increase_op k)
   where increase_op k (S s) = S (s+k)

gincreaseCompany :: Integer -> Company -> Company
gincreaseCompany k = G.transformBi (increase_op k)
   where increase_op k (S s) = S (s+k)

uincrone, gincrone :: String -> Integer -> Company -> Company
uincrone name k = U.descendBi $ f name k
    where
        f name k a@(D n _ _) | name == n = uincreaseAny k a
                             | otherwise = U.descend (f name k) a
gincrone name k = G.descendBi $ f name k
    where
        f name k a@(D n _ _) | name == n = G.transformBi (\(S s) -> S (s+k)) a
                             | otherwise = G.descend (f name k) a


ubill, gbill :: Company -> Integer
ubill x = sum [x | S x <- U.universeBi x]
gbill x = sum [x | S x <- G.universeBi x]


test :: IO ()
test = do
    let
      check :: (Eq a, Show a) => String -> (a, a) -> IO ()
      check msg (a, b)
        | a == b = return ()
        | otherwise = error $ unlines
          [ msg <> " failed:"
          , "a: " <> show a
          , "b: " <> show b
          ]
      (===) :: (Eq a, Show a) => a -> a -> IO ()
      (===) a b = check "case" (a, b)

    let expr1 = Add (Val 1) (Neg (Val 2))
    check "universe expr1"
      ( U.universe expr1
      , G.universe expr1
      )
    check "children expr1"
      ( U.children expr1
      , G.children expr1
      )
    check "transform expr1"
      ( U.transform (\x -> case x of Val n -> Val (n+1) ; _ -> x) expr1
      , G.transform (\x -> case x of Val n -> Val (n+1) ; _ -> x) expr1
      )

    let stmt11 = SAss (V "v") (EInt 1)
        stmt121 = SAss (V "x") (EInt 3)
        stmt12 = SReturn (EAdd (EInt 1) (EStm stmt121))
        stmt1 = SBlock [stmt11,stmt12]

    check "universe stmt1"
      ( U.universe stmt1
      , G.universe stmt1
      )
    check "children stmt1"
      ( U.children stmt1
      , G.children stmt1
      )
    check "childrenBi stmt1"
      ( U.childrenBi stmt1 :: [Exp]
      , G.childrenBi stmt1 :: [Exp]
      )

    check "universeBi stmt1"
      ( [i | EInt i <- U.universeBi stmt1]
      , [i | EInt i <- G.universeBi stmt1]
      )

    check "transformBi stmt1"
      ( U.transformBi (const ([] :: [Stm])) stmt1
      , G.transformBi (const ([] :: [Stm])) stmt1
      )
    check "descend stmt1"
      ( U.descend (const stmt121) stmt1
      , G.descend (const stmt121) stmt1
      )

    let str1 = "neil"
    check "universe str1"
      ( G.universe str1 :: [String]
      , ["neil","eil","il","l",""]
      )
    G.children str1 === ["eil"]
    G.universeBi str1 === "neil"
    G.transformBi (reverse :: String -> String) str1 === "elin"
    G.descendBi toUpper str1 === "NEIL"

    let eith1 = Left str1 :: Either String Int
    G.universeBi eith1 === ([] :: [Int])
    G.childrenBi eith1 === str1

    -- let mp1 = [Map.singleton "neil" (1::Int), Map.fromList [("morz",3),("test",4)], Map.empty]
    -- G.universeBi mp1 === [1::Int,3,4]
    -- G.universeBi (G.transformBi (+(1::Int)) mp1) === [2::Int,4,5]

    let com1 = C [D "test" (E (P "fred" "bob") (S 12)) []]
    G.universeBi com1 === [S 12]

    for_ testsExpr $ \expr -> check "simplify"
      ( usimplify expr
      , gsimplify expr
      )
    for_ testsExpr $ \expr -> check "variables"
      ( uvariables expr
      , gvariables expr
      )
    for_ testsExpr $ \expr -> check "zeros"
      ( uzeros expr
      , gzeros expr
      )
    for_ testsStm $ \stm -> check "constFold"
      ( uconstFold stm
      , gconstFold stm
      )
    for_ testsStm $ \stm -> check "rename"
      ( urename stm
      , grename stm
      )
    for_ testsStm $ \stm -> check "symbols"
      ( usymbols stm
      , gsymbols stm
      )
    for_ testsPar $ \par -> check "bill"
      ( ubill par
      , gbill par
      )
    for_ testsPar $ \par -> check "increase800"
      ( uincrease 800 par
      , gincrease 800 par
      )
    for_ testsPar $ \par -> check "increase1500"
      ( uincrease 1500 par
      , gincrease 1500 par
      )
