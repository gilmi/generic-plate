-- Taken from the uniplate library by Neil Mitchell

{-# LANGUAGE FlexibleContexts, TypeApplications, LambdaCase, DeriveGeneric, DeriveDataTypeable, DeriveAnyClass #-}

import Type
import Testset
import Data.Data
import Control.DeepSeq
import GHC.Generics
import Data.Foldable (for_)
import Criterion.Main

import qualified Data.Generics.Uniplate.Data as U
import qualified Data.Generics.Plated as G

main :: IO ()
main = (test *>) $ defaultMain
  [ bgroup "uniplate"
    [ bgroup "simplify"
      [ bench "generic-plate" $ nf (map gsimplify) testsExpr
      , bench "uniplate"      $ nf (map usimplify) testsExpr
      ]
    , bgroup "variables"
      [ bench "generic-plate" $ nf (map gvariables) testsExpr
      , bench "uniplate"      $ nf (map uvariables) testsExpr
      ]
    , bgroup "zeros"
      [ bench "generic-plate" $ nf (map gzeros) testsExpr
      , bench "uniplate"      $ nf (map uzeros) testsExpr
      ]
    , bgroup "constFold"
      [ bench "generic-plate" $ nf (map gconstFold) testsStm
      , bench "uniplate"      $ nf (map uconstFold) testsStm
      ]
    , bgroup "rename"
      [ bench "generic-plate" $ nf (map grename) testsStm
      , bench "uniplate"      $ nf (map urename) testsStm
      ]
    , bgroup "symbols"
      [ bench "generic-plate" $ nf (map gsymbols) testsStm
      , bench "uniplate"      $ nf (map usymbols) testsStm
      ]
    , bgroup "bill"
      [ bench "generic-plate" $ nf (map gbill) testsPar
      , bench "uniplate"      $ nf (map ubill) testsPar
      ]
    , bgroup "increase800"
      [ bench "generic-plate" $ nf (map $ gincrease 800) testsPar
      , bench "uniplate"      $ nf (map $ uincrease 800) testsPar
      ]
    ]
  ]

uvariables, gvariables :: Expr -> [String]
uvariables x = [y | Var y <- U.universe x]
gvariables x = [y | Var y <- G.universe x]

uzeros, gzeros :: Expr -> Int
uzeros x = length [() | Div _ (Val 0) <- U.universe x]
gzeros x = length [() | Div _ (Val 0) <- G.universe x]

usimplify, gsimplify :: Expr -> Expr
usimplify = U.transform simp
gsimplify = G.transform simp

simp (Sub x y)          = simp $ Add x (Neg y)
simp (Add x y) | x == y = Mul (Val 2) x
simp x                  = x

urename, grename :: Stm -> Stm
urename = U.transformBi rename_op
    where rename_op (V x) = V ("_" ++ x)
grename = G.transformBi rename_op
    where rename_op (V x) = V ("_" ++ x)

usymbols, gsymbols :: Stm -> [(Var,Typ)]
usymbols x = [(v,t) | SDecl t v <- U.universeBi x]
gsymbols x = [(v,t) | SDecl t v <- G.universeBi x]

uconstFold, gconstFold :: Stm -> Stm
uconstFold = U.transformBi const_op
    where
        const_op (EAdd (EInt n) (EInt m)) = EInt (n+m)
        const_op x = x
gconstFold = G.transformBi const_op
    where
        const_op (EAdd (EInt n) (EInt m)) = EInt (n+m)
        const_op x = x

uincrease, gincrease :: Integer -> Company -> Company
uincrease = uincreaseAny
gincrease = gincreaseCompany

uincreaseAny :: U.Biplate a Salary => Integer -> a -> a
uincreaseAny k = U.transformBi (increase_op k)
   where increase_op k (S s) = S (s+k)

gincreaseCompany :: Integer -> Company -> Company
gincreaseCompany k = G.transformBi (increase_op k)
   where increase_op k (S s) = S (s+k)

uincrone, gincrone :: String -> Integer -> Company -> Company
uincrone name k = U.descendBi $ f name k
    where
        f name k a@(D n _ _) | name == n = uincreaseAny k a
                             | otherwise = U.descend (f name k) a
gincrone name k = G.descendBi $ f name k
    where
        f name k a@(D n _ _) | name == n = G.transformBi (\(S s) -> S (s+k)) a
                             | otherwise = G.descend (f name k) a


ubill, gbill :: Company -> Integer
ubill x = sum [x | S x <- U.universeBi x]
gbill x = sum [x | S x <- G.universeBi x]


test :: IO ()
test = do
    let
      check :: (Eq a, Show a) => String -> (a, a) -> IO ()
      check msg (a, b)
        | a == b = return ()
        | otherwise = error $ unlines
          [ msg <> " failed:"
          , "a: " <> show a
          , "b: " <> show b
          ]

    for_ testsExpr $ \expr -> check "simplify"
      ( usimplify expr
      , gsimplify expr
      )
    for_ testsExpr $ \expr -> check "variables"
      ( uvariables expr
      , gvariables expr
      )
    for_ testsExpr $ \expr -> check "zeros"
      ( uzeros expr
      , gzeros expr
      )
    for_ testsStm $ \stm -> check "constFold"
      ( uconstFold stm
      , gconstFold stm
      )
    for_ testsStm $ \stm -> check "rename"
      ( urename stm
      , grename stm
      )
    for_ testsStm $ \stm -> check "symbols"
      ( usymbols stm
      , gsymbols stm
      )
    for_ testsPar $ \par -> check "bill"
      ( ubill par
      , gbill par
      )
    for_ testsPar $ \par -> check "increase800"
      ( uincrease 800 par
      , gincrease 800 par
      )
    for_ testsPar $ \par -> check "increase1500"
      ( uincrease 1500 par
      , gincrease 1500 par
      )

